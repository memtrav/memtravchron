<?php

namespace App\Http\Controllers;

use GuzzleHttp\Pool;
use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Request;
use GuzzleHttp\Psr7\Response;
use App\Countries;
use DB;
use Zend;
use DateTime;
use Guzzle\Http\Exception\ClientErrorResponseException;
use GuzzleHttp\Exception\BadResponseException;

class ChronController extends Controller
{
    public function getHTB()
    {
    	ini_set('memory_limit','2560M');
		ini_set('max_execution_time', 90000);
		set_time_limit(90000);
    	$responsedata = array();
    	$catch = array();
    	for($x = 5000; $x < 75001; $x+=5000){
    		array_push($catch, $x);
    	}
    	$client = new Client;
    	
    	for($i = 0; $i <= 75000; $i++)
    	{
	    	$endpoint = "https://api.hotelbeds.com/hotel-content-api/1.0/hotels/$i?language=ENG";
	    	$apiKey =  "6dkxckesy858wcghpmdcw3sd";
	        $sharedSecret = "FVfNRnnGwn";
	        $signature = hash("sha256", $apiKey.$sharedSecret.time());
			$headers = ["Api-Key" => $apiKey, "X-Signature" => $signature, "Accept" =>"application/JSON"];
			$request = new request('GET', $endpoint, ["Api-Key" => $apiKey, "X-Signature" => $signature, "Accept" => "application/JSON"]);
	        try 
	        {
	        	$response = $client->send($request);
	            $body = $response->getBody();
	           	$hotel = json_decode($body, true);
	           	array_push($responsedata, $hotel);
	        }
	        catch (BadResponseException $e) 
	        {
	        	//print_r($e->getResponse()->getBody()->getContents());
	        }		
	        echo "Hotel: $i <br>";
	    	if(in_array($i, $catch)){
	    		$fp = fopen(base_path(). "/public/db/htb/hotels-$i.json", 'w');
				fwrite($fp, json_encode($responsedata, 128));
    		}
    	}
	}

	public function getHTBHotels(){
		ini_set('memory_limit','5560M');
		ini_set('max_execution_time', 90000);
		set_time_limit(90000);
    	$responsedata = array();
		$catch = array();
    	for($x = 5000; $x < 75001; $x+=5000){
    		array_push($catch, $x);
    	}
    	foreach ($catch as $range) {
    		$hotels = json_decode(file_get_contents(base_path(). "/public/htb/hotels-$range.json"), true);
    		foreach ($hotels as $hotel) {
    			if(array_key_exists("hotel", $hotel)){
    				$results = array("name" => array(), "code" => array());
    				$results["name"] = $hotel["hotel"]["name"]["content"];
    				$results["code"] = $hotel["hotel"]["code"];
    				array_push($responsedata, $results);
    				unset($results);
    			}
    		}
    	}
    	$c = json_encode($responsedata, 128);
    	$fp = fopen(base_path(). "/public/htb/mappingfile.json", 'w');
		fwrite($fp, $c);
	}
}
